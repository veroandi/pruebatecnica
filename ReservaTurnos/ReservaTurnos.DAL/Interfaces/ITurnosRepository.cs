﻿using Domain.ReservaTurnos.Common;
using System.Threading.Tasks;

namespace Domain.ReservaTurnos.DAL.Interfaces
{
    public interface ITurnosRepository
    {        
        Task<string> CrearTurnos(TurnosDto turnos);
    }
}
