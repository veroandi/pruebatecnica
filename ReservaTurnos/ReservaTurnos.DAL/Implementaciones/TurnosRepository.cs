﻿using Domain.ReservaTurnos.Common;
using Domain.ReservaTurnos.DAL.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Domain.ReservaTurnos.DAL.Implementaciones
{
    public class TurnosRepository : ITurnosRepository
    {
        #region Propiedades
        private IConfiguration Configuration;
        private readonly string ConnectionString;
        #endregion Propiedades
        
        public TurnosRepository(IConfiguration configuration)
        {
            this.Configuration = configuration;
            this.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
        }

        public Task<string> CrearTurnos(TurnosDto turnos)
        {
                string respuesta = string.Empty;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = connection.CreateCommand();
                    try
                    {                    
                        cmd.CommandText = "develop.CrearTurnos";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@FechaInicio", SqlDbType.DateTime));
                        cmd.Parameters["@FechaInicio"].Value = turnos.FechaInicio;
                        cmd.Parameters.Add(new SqlParameter("@FechaFin", SqlDbType.DateTime));
                        cmd.Parameters["@FechaFin"].Value = turnos.FechaFin;
                        cmd.Parameters.Add(new SqlParameter("@IdServicio", SqlDbType.Int));
                        cmd.Parameters["@IdServicio"].Value = turnos.IdServicio;
                        cmd.Parameters.Add(new SqlParameter("@Respuesta", SqlDbType.VarChar,4000));
                        cmd.Parameters["@Respuesta"].Direction = ParameterDirection.Output;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        respuesta = cmd.Parameters["@Respuesta"].Value.ToString();                    
                    }
                    catch (ArgumentException ex)
                    {
                        respuesta = ex.Message;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return Task.FromResult(respuesta);
                } 
        }
    }
}
