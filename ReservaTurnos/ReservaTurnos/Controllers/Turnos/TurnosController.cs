﻿using System;
using System.Threading.Tasks;
using Domain.ReservaTurnos.BL.Interfaces;
using Domain.ReservaTurnos.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Domain.ReservaTurnos.API.Controllers.Turnos
{
    [Route("api/[controller]")]
    [ApiController]
    public class TurnosController : ControllerBase
    {
        private readonly ITurnosBL turnosBL;
        public TurnosController(ITurnosBL turnosBL)
        {
            this.turnosBL = turnosBL;
        }
       
        [HttpPost("CrearTurnos")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> CrearTurnos(TurnosDto crearTurno)
        {          
            try
            {
                // Validación del modelo
                if (!ModelState.IsValid)               
                    return BadRequest("Error en datos del modelo");
                var response = await turnosBL.CrearTurnos(crearTurno);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);                
            }
        }
    }
}