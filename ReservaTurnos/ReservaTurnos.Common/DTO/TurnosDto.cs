﻿using System;

namespace Domain.ReservaTurnos.Common
{    
    public class TurnosDto
    {        
        public int IdServicio { get; set; }
        
        public DateTime FechaInicio { get; set; }
        
        public DateTime FechaFin { get; set; }
        
        public TurnosDto() { }
        
        public TurnosDto(int idServicio, DateTime fechaInicio, DateTime fechaFin)
        {
            this.IdServicio = idServicio;
            this.FechaInicio = fechaInicio;
            this.FechaFin = fechaFin;
        }
    }
}
