﻿using Domain.ReservaTurnos.Common;
using System.Threading.Tasks;

namespace Domain.ReservaTurnos.BL.Interfaces
{
    public interface ITurnosBL
    {
        Task<string> CrearTurnos(TurnosDto crearTurno);
    }
}
