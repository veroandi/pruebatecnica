﻿#region Referecias
using Domain.ReservaTurnos.DAL.Interfaces;
using Domain.ReservaTurnos.DAL.Implementaciones;
using Microsoft.Extensions.DependencyInjection;
#endregion Referencias

namespace Domain.ReservaTurnos.BussinesLayer
{
    public class StartupBL
    {
        public static void ConfigureServices(IServiceCollection services, string connectionString)
        {
            services.AddScoped<ITurnosRepository, TurnosRepository>();            
        }
    }
}
