﻿#region Referencias
using Domain.ReservaTurnos.BL.Interfaces;
using Domain.ReservaTurnos.Common;
using Domain.ReservaTurnos.DAL.Interfaces;
using System;
using System.Threading.Tasks;
#endregion Referencias

namespace Domain.ReservaTurnos.TurnosBL.Implementaciones
{
    public class TurnosBL : ITurnosBL
    {
        private readonly ITurnosRepository turnosRepository;
        public TurnosBL(ITurnosRepository turnosRepository) 
        {
            this.turnosRepository = turnosRepository;
        }

        public async Task<string> CrearTurnos(TurnosDto turnos)
        {
            try
            {
                return await this.turnosRepository.CrearTurnos(turnos);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            }
        }

    }
}
