CREATE PROCEDURE [develop].[CrearTurnos]	@FechaInicio DATETIME, 
										@FechaFin DATETIME,
										@IdServicio INT,
										@Respuesta varchar(4000) out
AS
BEGIN
	-- Variables generales
	BEGIN
		DECLARE @existe_servicio INT
		DECLARE @duracion INT
		DECLARE @hora_apertura_aux INT
		DECLARE @hora_cierre_aux INT
		DECLARE @fecha_inicio_aux DATETIME = @FechaInicio
		DECLARE @fecha_fin_aux DATETIME = @FechaFin
		DECLARE @dias INT
		DECLARE @dias_aux INT = 0
		DECLARE @turno_ INT = 0
		DECLARE @turno_aux INT = 0
		DECLARE @turnos_generados INT = 0
		DECLARE @FECHA_HORA_TURNO_INICIAL DATETIME
		DECLARE @FECHA_HORA_TURNO_FINAL DATETIME
		DECLARE @HORA_TURNO_INICIAL TIME
		DECLARE @HORA_TURNO_FINAL TIME
		DECLARE @ID_TURNO INT = 0
		DECLARE @ID_TURNOS_GENERADOS VARCHAR(4000) = ''
	END -- Fin Variables generales
	BEGIN
		BEGIN -- Validacion  de fechas
			IF @FechaFin < @FechaInicio
			BEGIN 
				SET @Respuesta = 'La fecha final debe ser mayor a la fecha inicial.'
				RETURN
			END
		END -- Fin Validacion de fechas
		BEGIN -- Validacion del identificador del servicio
			BEGIN TRY -- Control de ejecución de bloque 
				SELECT @existe_servicio = CAST(COUNT(1)  AS INT)
				FROM develop.servicios 
				WHERE id_servicio = @IdServicio
				PRINT 'Existe servicio: ' + + CAST(@existe_servicio as varchar(1000))
				IF @existe_servicio = 0
				BEGIN 
					SET @Respuesta = 'El Id del servicio seleccionado no existe en la base de datos: ' + + CAST(@IdServicio as varchar(1000))
					RETURN
				END						
			END TRY  
			BEGIN CATCH
				SET @Respuesta = 'Ocurrió un error consultando el identificador del servicio.'
				RETURN
			END CATCH
		END -- Fin Validacion del identificador del servicio
		BEGIN -- Consultar la hora de apertura, la hora de cierre y la duración del servicio
			BEGIN TRY -- Control de errores  
				SELECT @hora_apertura_aux = LTRIM(DATEDIFF(HOUR, 0, hora_apertura)),
					   @duracion = duracion,
					   @hora_cierre_aux = LTRIM(DATEDIFF(HOUR, 0, hora_cierre))
				FROM develop.servicios  
				WHERE id_servicio = @IdServicio		
			END TRY  
			BEGIN CATCH
				SET @Respuesta = 'Ocurrió un error consultando la hora de apertura, la hora de cierre y la duración del servicio.'
				RETURN @Respuesta
			END CATCH
		END -- Consultar la hora de apertura, la hora de cierre y la duración del servicio
		BEGIN			
			BEGIN TRY -- Control de errores
				PRINT 'hora_apertura_aux: '  +  + CAST(@hora_apertura_aux as varchar(1000))
				PRINT 'hora_cierre_aux: '  +  + CAST(@hora_cierre_aux as varchar(1000))
				SET @fecha_inicio_aux = (DATEADD(HOUR,@hora_apertura_aux,@FechaInicio))
				SET @fecha_fin_aux = (DATEADD(HOUR,@hora_cierre_aux,@FechaFin))
				PRINT 'fecha hora inicio: '  +  + CAST(@fecha_inicio_aux as varchar(1000))
				PRINT 'fecha hora fin: '  +  + CAST(@fecha_fin_aux as varchar(1000))

				SET @dias = (SELECT DATEDIFF(DAY, @FechaInicio, @FechaFin)) + 1
				PRINT 'total dias a calcular: ' +  + CAST(@dias as varchar(1000))

				SET @turno_ = (SELECT DATEDIFF(MINUTE, hora_apertura, hora_cierre)/duracion FROM develop.servicios WHERE id_servicio = @IdServicio)
				PRINT 'cantidad de turnos por dia: ' + + CAST(@turno_ as varchar(1000)) 
				WHILE (@dias_aux < @dias)
				BEGIN -- Ciclo de dias
					PRINT '-- DIA N°:' + CAST((@dias_aux + 1) as varchar(1000))
					SET @turno_aux= 0
					WHILE (@turno_aux< @turno_)
					BEGIN	-- Ciclo de turnos
						PRINT '-- TURNO N°:' + CAST((@turno_aux + 1) as varchar(1000))
						PRINT 'duracion del turno: ' + + CAST(@duracion as varchar(1000))
						SET @FECHA_HORA_TURNO_INICIAL = @fecha_inicio_aux
						SET @FECHA_HORA_TURNO_FINAL = (DATEADD(MINUTE,@duracion,@FECHA_HORA_TURNO_INICIAL))
						SET @HORA_TURNO_INICIAL = (SELECT LTRIM(RIGHT(CONVERT(VARCHAR(20), @FECHA_HORA_TURNO_INICIAL, 100), 7)))
						SET @HORA_TURNO_FINAL = (SELECT LTRIM(RIGHT(CONVERT(VARCHAR(20), @FECHA_HORA_TURNO_FINAL, 100), 7)))
						PRINT '@FECHA_HORA_TURNO_INICIAL: ' + + CAST(@FECHA_HORA_TURNO_INICIAL as varchar(1000))
						PRINT '@FECHA_HORA_TURNO_FINAL: ' + + CAST(@FECHA_HORA_TURNO_FINAL as varchar(1000))

						INSERT INTO develop.turnos (id_servicio, fecha_turno, hora_inicio, hora_fin)
				        VALUES (@IdServicio, Convert(date, @FECHA_HORA_TURNO_INICIAL) ,@HORA_TURNO_INICIAL,@HORA_TURNO_FINAL)
						SET @ID_TURNO = SCOPE_IDENTITY()
						PRINT 'ID TURNO: ' + + CAST(@ID_TURNO as varchar(1000))
						    IF (@ID_TURNOS_GENERADOS <> '')
							BEGIN
								SET @ID_TURNOS_GENERADOS = @ID_TURNOS_GENERADOS + ',' + CAST(@ID_TURNO as varchar(1000))
							END
							ELSE BEGIN
								SET @ID_TURNOS_GENERADOS = CAST(@ID_TURNO as varchar(1000))
							END
						SET @fecha_inicio_aux = @FECHA_HORA_TURNO_FINAL
						SET @turno_aux= @turno_aux+ 1
						SET @turnos_generados= @turnos_generados+ 1
					END
					SET @dias_aux = @dias_aux + 1
					SET @fecha_inicio_aux = (SELECT DATEADD(DAY,@dias_aux,(DATEADD(HOUR,@hora_apertura_aux,@FechaInicio)))) 
				END -- Fin Ciclos de dias
				SET @Respuesta = 'Numero de turnos creados: ' + CAST(@turnos_generados as varchar(1000)) + ', Listado de IDS de turnos generados: ' + CAST(@ID_TURNOS_GENERADOS as varchar(1000))
			END TRY  
			BEGIN CATCH
				SET @Respuesta = 'Ocurrió un error en el procedimiento de CrearTurnos.'
				RETURN
			END CATCH
		END 
	END
END
GO