CREATE SCHEMA [develop]
GO

/* Creaci�n de la tabla comercios */
CREATE TABLE develop.comercios (
    id_comercio INT PRIMARY KEY IDENTITY (1, 1),
    nom_comercio VARCHAR (100) NOT NULL,
	aforo_maximo INT NOT NULL,	    
    estado_logico VARCHAR(20) NOT NULL DEFAULT 'Activo',
	CONSTRAINT estado_logico_comercios CHECK (estado_logico = 'Activo' OR estado_logico ='Inactivo')
);


/* Creaci�n de la tabla servicios comerciales */
CREATE TABLE develop.servicios  (
    id_servicio INT PRIMARY KEY IDENTITY (1, 1),
	id_comercio INT NOT NULL,
	nom_servicio VARCHAR (50) NOT NULL,
	hora_apertura TIME NOT NULL,	
	hora_cierre TIME NOT NULL,
	duracion INT NOT NULL,
    estado_logico VARCHAR(20) NOT NULL DEFAULT 'Activo',
	CONSTRAINT estado_logico_servicios CHECK (estado_logico = 'Activo' OR estado_logico ='Inactivo'),
	FOREIGN KEY (id_comercio) REFERENCES develop.comercios (id_comercio),
);

/* Creaci�n de la tabla servicios turnos */
CREATE TABLE develop.turnos  (
    id_turno INT PRIMARY KEY IDENTITY (1, 1),
	id_servicio INT NOT NULL,
	fecha_turno DATETIME NOT NULL,	
	hora_inicio TIME NOT NULL,
	hora_fin TIME NOT NULL,
	estado VARCHAR(20) NOT NULL DEFAULT 'Creado',
    estado_logico VARCHAR(20) NOT NULL DEFAULT 'Activo',
	CONSTRAINT estado_turnos CHECK (estado = 'Creado' OR estado ='Anulado' OR estado ='Finalizado' ),
	CONSTRAINT estado_logico_turnos CHECK (estado_logico = 'Activo' OR estado_logico ='Inactivo'),	
	FOREIGN KEY (id_servicio) REFERENCES develop.servicios (id_servicio),
);

/* Datos semilllas */
INSERT INTO develop.comercios (nom_comercio, aforo_maximo)
VALUES ('UNICENTRO',35);
INSERT INTO develop.comercios (nom_comercio, aforo_maximo)
VALUES ('VILLACENTRO',50);
INSERT INTO develop.comercios (nom_comercio, aforo_maximo)
VALUES ('PRIMAVERA',43);
INSERT INTO develop.servicios (id_comercio, nom_servicio, hora_apertura, hora_cierre, duracion)
VALUES (1,'CINE','2021-10-22T01:00:00.000Z','2021-10-22T23:00:00.000Z',30)
INSERT INTO develop.servicios (id_comercio, nom_servicio, hora_apertura, hora_cierre, duracion)
VALUES (2,'CINE','2021-10-22T01:00:00.000Z','2021-10-22T23:00:00.000Z',30)
INSERT INTO develop.servicios (id_comercio, nom_servicio, hora_apertura, hora_cierre, duracion)
VALUES (3,'CINE','2021-10-22T01:00:00.000Z','2021-10-22T23:00:00.000Z',30)

